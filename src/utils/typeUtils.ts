import { InvalidString } from "../Components/interfaces";

class StringUtils {
	static stringIsEmpty = (s: InvalidString): boolean => {
		return !s || s.length === 0 || s === "";
	};
}


const objectIsEmpty = (o?: Object): boolean => {
	return o === null || o === undefined || Object.keys(o).length === 0;
}

export { StringUtils, objectIsEmpty};
