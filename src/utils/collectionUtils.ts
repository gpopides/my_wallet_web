import { of } from "rxjs";
import { map, reduce } from "rxjs/operators";
import moment from "moment";

export enum SORT_TYPES {
	STRING = "string",
	NUMBER = "number",
	DATE = "date"
}

const sortStringsOrNumbers = (a: string | number, b: string | number) => {
	if (a > b) return -1;
	if (a < b) return 1;
	return 0;
};

const sortDates = (a: string, b: string) => {
	if (moment(a).isAfter(moment(b))) return -1;
	if (moment(a).isBefore(moment(b))) return 1;
	return 0;
};

export const sumCollection = <T, K extends keyof T>(
	collection: Array<T>,
	property: K
): number | null => {
	let _sum: number | null = null;

	of(...collection)
		.pipe(
			map((x: T) => x[property]),
			reduce((a: any, b: any) => a + b, 0)
		)
		.subscribe((sum: number) => (_sum = sum));

	return _sum;
};

export const orderBy = <T>(
	collection: Array<T>,
	attribute: keyof T,
	sortType: SORT_TYPES
): Array<T> => {
	const sortFn =
		sortType === SORT_TYPES.STRING || sortType === SORT_TYPES.NUMBER
			? sortStringsOrNumbers
			: sortDates;

	return collection.sort((a: any, b: any) =>
		sortFn(a[attribute], b[attribute])
	);
};
