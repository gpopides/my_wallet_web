import {useState} from "react";

function useFormFields<T>(initFormValues: T): Array<T | any> {
	const [formFields, updateFormField] = useState<T>(initFormValues);

		// use this to update a specific field or the whole state object 
		const handleFormFieldChange = (replaceObject: boolean = false, existingObject: T | null, field: string, value: string | number) => {
			if (!replaceObject) {
				updateFormField((ps: T) => ({
					...ps,
					[field]: value
				}))
			} else if (existingObject){
				updateFormField(existingObject);
			}
		}

		return [formFields, handleFormFieldChange];
}

const useBoolean = (initBoolState: boolean): Array<Boolean | any> => {
	const [v, updateV] = useState(initBoolState);

	const triggerBoolean = () => {
		updateV((ps: boolean) => !ps);
	}

	return [v, triggerBoolean];
}


export { useFormFields, useBoolean};
