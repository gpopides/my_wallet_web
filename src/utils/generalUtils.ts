//import colors from "./colorNames";
import moment from "moment";
import { IState, ObjectWithIdentifier } from "../store/interfaces";
import { store } from "../store/store";

const BASE_URL = "http://localhost:8080/api/wallets/";

const CATEGORY_COLORS: {[key: string]: string}= {
	groceries: "orange",
	drink: "blue",
	subscriptions: "red",
	fastFood: "cyan"
}

interface SumType {[key: string]: any};

export const DATE_FORMAT: string = "YYYY-MM-DD";

export const getSumOfList = <T extends SumType> (items: Array<T>, property: keyof T, precision: number = 2): number => {
	return parseFloat(items
		.map((i: T) => i[property])
		.reduce((a: number, b: number) => a + b, 0)
		.toFixed(precision));
}

const getRandomColorIndex = (colors: Array<string>) => {
	return Math.floor(Math.random() * (colors.length - 0) + 0);
}

export const getRandomColor = () => {
	const colors: Array<String> = ["red", "orange", "yellow", "green", "cyan", "blue", "violet"];
	const index = getRandomColorIndex(Object.keys(colors));
	return colors[index];
}

export const pipe = <T> (...fns: Array<T>) => {
  return (arg: T) => fns.reduce((prev: any, fn: any) => fn(prev), arg);
}

export const generateRandomId = (stringLength: number): string => {
	return Math.random().toString(36).substring(stringLength);
}

export const DEFAULT_DATE: string = moment().format(DATE_FORMAT);

export const getCurrentState = (): IState => {
	return store.getState();
}



export const getTagColor = (category: string | null) => {
	if (category) 
		return CATEGORY_COLORS[category];
}

export const deleteEntityUrlMappings: {[key: string]: string} = {
	expense: BASE_URL + "expenses"
}

export const replaceObjectInArray = <T extends ObjectWithIdentifier> (items: Array<T>, newItem: T) => {
	return [
		...items.filter(x => x.id !== newItem.id),
		newItem
	]
}
