import { IAuthState } from "./auth/authReducer";
import { IWalletState } from "./wallet/reducer";
import {Expense} from "./wallet/interfaces";

export interface BaseAction {
	type: string;
}

export interface BaseEpicResult {
	type: string;
	payload?: any;
	extraPayload?: any; // use this to pass data between epics that will not be stored in state
}

export interface IState {
	auth: IAuthState;
	wallet: IWalletState;
}

export interface BaseResponse {
	error: string;
}

export enum ModifyActions {
	EDIT = "EDIT",
	DELETE = "DELETE"
}

export type ModifyAction = ModifyActions.DELETE | ModifyActions.EDIT;

export interface ModifyEntityPayload {
	id: number | string;
	data?: any;
}

export type ObjectWithIdentifier = {
	id?: string | number;
}

export interface RequestHeaders {[key:string]: string}

export type ENTITIES = Expense;
