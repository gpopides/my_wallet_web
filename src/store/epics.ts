import {combineEpics} from "redux-observable";
import authEpics from "./auth/epic";
import walletEpics from "./wallet/epic";
import generalEpics from "./general/epic";

const epics = combineEpics(...authEpics, ...walletEpics, ...generalEpics);
export default epics;
