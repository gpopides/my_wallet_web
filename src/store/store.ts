import { applyMiddleware, createStore } from "redux";
import rootReducer from "./reducer";
import { createEpicMiddleware } from "redux-observable";
import { composeWithDevTools } from "redux-devtools-extension";
import rootEpic from "./epics";
import { createBrowserHistory } from "history";
import { routerMiddleware } from "connected-react-router";
import {persistStore} from "redux-persist";

const epicMiddleware = createEpicMiddleware();

export const history = createBrowserHistory();

const store = createStore(
	rootReducer(history),
	composeWithDevTools(
		applyMiddleware(routerMiddleware(history), epicMiddleware)
	)
);



epicMiddleware.run(rootEpic);

let persistor = persistStore(store)


export {store, persistor}
