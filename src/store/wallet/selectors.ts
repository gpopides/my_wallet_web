import { createSelector } from "reselect";
import { IState } from "../interfaces";
import moment from "moment";
import {DATE_FORMAT} from "../../utils/generalUtils";
import {Expense} from "./interfaces";
import {orderBy, SORT_TYPES} from "../../utils/collectionUtils";

export enum ExpenseFilter {
	DAY = "day",
	MONTH = "month",
	WEEK = "week"
}

interface IGrouppedExpenseSelector {
	expenses: IGrouppedExpenses;
	filter: ExpenseFilter;
}

interface IGrouppedExpenses {
	[key:string]: Array<Expense>
}



const getGrouppedExpenses = (
	state: IState,
	filter: ExpenseFilter
): IGrouppedExpenseSelector => ({
	expenses: state.wallet.grouppedExpenses,
	filter
});

const latestExpenses = (state: IState) => state.wallet.latestExpenses;

export const getFilteredExpenses = createSelector(
	getGrouppedExpenses,
	(groupped: IGrouppedExpenseSelector): Array<Expense> => {
		const filterMethodMapping: {[key: string]: any} = {
			"day": filterByDay,
			"week": filterByWeek,
			"month": filterByMonth,
		}
		return filterMethodMapping[groupped.filter](groupped.expenses);
	}
);

export const getLatestExpenses = createSelector(
	latestExpenses,
	(expenses: Array<Expense>): Array<Expense> => {
		return orderBy(expenses, "date", SORT_TYPES.DATE);
	}
);

const filterByDay = (expenses: IGrouppedExpenses): Array<Expense> => {
	let filteredExpenses: Array<Expense> = [];
	const currentDay: string = moment().format(DATE_FORMAT);

	if (expenses[currentDay]) {
		filteredExpenses = expenses[moment().format(DATE_FORMAT)];
	}
	return filteredExpenses.length ? orderBy(filteredExpenses, "date", SORT_TYPES.DATE) : [];
}

const filterByWeek = (expensesPerDate: IGrouppedExpenses): Array<Expense> => {
	let filteredExpenses: Array<Expense> = [];
	const todaysDate = moment();
	const firstDayOfTheWeek = moment().startOf("week");

	for (const [date, _expenses] of Object.entries(expensesPerDate)) {
		const dateAsMomentObject = moment(date);

		if (dateAsMomentObject.isBetween(firstDayOfTheWeek, todaysDate, "days", "[]")) {
			filteredExpenses.push(..._expenses);
		}
	}
	return orderBy(filteredExpenses, "date", SORT_TYPES.DATE);
}

const filterByMonth = (expensesPerDate: IGrouppedExpenses): Array<Expense> => {
	const filteredExpenses: Array<Expense> = [];

	const todaysDate = moment();
	const beggingnOfTheMonth = moment().startOf("month");

	for (const [date, _expenses] of Object.entries(expensesPerDate)) {
		const dateAsMomentObject = moment(date);

		if (dateAsMomentObject.isBetween(beggingnOfTheMonth, todaysDate, "days", "[]")) {
			filteredExpenses.push(..._expenses);
		}
	}
	return orderBy(filteredExpenses, "date", SORT_TYPES.DATE);
}

