import actions from "./actions";
import {BaseAction, ModifyEntityPayload, ModifyActions, BaseResponse} from "../interfaces";

export interface ITopUpData {
	amount: number;
	message: string;
}

export interface WalletTopUpAction extends BaseAction {
	//type: typeof actions.TOP_UP_REQUEST,
	topUpData: ITopUpData
	walletId: number;
}

export interface GetWalletBalanceAction extends BaseAction{
	walletId: number;
}

export interface GetExpensesAction {
	type: string;
	walletId: number;
}

export interface NewExpenseAction extends BaseAction {
	expense: Expense
}

export interface ModifyExpenseAction extends BaseAction {
	modificationAction: ModifyActions;
	payload: ModifyEntityPayload;
}

export interface WalletTopUpResponse {
	updatedWallet: {
		balance: number
	}
	error?: string
}

export interface TopUpResponseAction {
	type: typeof actions.TOP_UP_RESPONSE
	balance: number;
}

export interface WalletTopUpRequest  {
	amount: number;
	reason: string;
}

export interface Wallet {
	id: number;
	balance: number;
}


export type Expense = {
	id?: number;
	date: string;
	description?: string | null;
	amount: number;
	category: string | null;
	walletId: number;
}

export interface GetExpensesResponse {
	result: Expense
}

export interface NewExpenseResponse extends BaseResponse {
	expense: Expense;
}

export interface GetWalletBalanceResponse {
	balance: number;
}

export interface DeleteExpenseResponse extends BaseResponse {
	deleteSuccess: boolean;
	balance?: number;
	entity?: Expense;
}

export interface ServiceResult {
	success: boolean;
	payload?: any;
	error?: any;
}

export interface DeleteExpenseResult {
	deleteSuccess: boolean;
	balance?: number;
}

export interface UpdateExpenseResult {
	updateSuccess: boolean;
	expense?: Expense;
}

export interface UpdateExpenseResponse  {
	entity?: Expense
}

export interface GrouppedExpensesResult {
	[key: string]: Array<Expense>
}

export interface ActionPayload extends WalletTopUpAction, TopUpResponseAction { payload: any}
