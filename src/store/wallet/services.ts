import {  AjaxError } from "rxjs/ajax";
import { map, catchError } from "rxjs/operators";
import {
	Expense,
	GetWalletBalanceResponse,
	GetExpensesResponse,
	NewExpenseResponse,
	DeleteExpenseResult,
	DeleteExpenseResponse,
	UpdateExpenseResult,
	UpdateExpenseResponse,
	GrouppedExpensesResult
} from "./interfaces";
import { Observable, of } from "rxjs";
import HttpClient, { IHttpResponse } from "../../httpClient";

const BASE_URL = "http://localhost:4000/api/wallets";

const getWalletBalanceService = (walletId: number) => {
	const url: string = `${BASE_URL}/${walletId}/balance`;

	return HttpClient.get_r<GetWalletBalanceResponse>(url).pipe(
		map((r: IHttpResponse<GetWalletBalanceResponse>) => {
			const response: GetWalletBalanceResponse = r.body!;
			return { success: true, payload: { balance: response.balance } };
		}),
		catchError((err: AjaxError): any => {
			console.log(err);
			return { success: true, error: err };
		})
	);
};

const getExpensesService = (walletId: number) => {
	const url = `${BASE_URL}/${walletId}/expenses`;
	return HttpClient.get_r<GetExpensesResponse>(url).pipe(
		map((requestResult: IHttpResponse<GetExpensesResponse>) => {
			if (!requestResult.error) {
				const response: GetExpensesResponse = requestResult.body!;
				return { success: true, payload: { latestExpenses: response.result } };
			} else {
				return of({ success: false, error: requestResult.errorMsg });
			}
		})
	);
};

const addNewExpenseService = (expense: Expense) => {
	const url = `${BASE_URL}/addExpense`;

	return HttpClient.post_r<NewExpenseResponse>(url, expense).pipe(
		map((result: IHttpResponse<NewExpenseResponse>) => {
			const response: NewExpenseResponse = result.body!;

			return !response.error
				? { success: true, expense: response.expense }
				: { success: false };
		}),
		catchError((er: AjaxError): any => {
			console.log(er);
			return { success: false };
		})
	);
};

const deleteExpenseService = (url: string): Observable<DeleteExpenseResult> => {
	return HttpClient.delete_r<DeleteExpenseResponse>(url).pipe(
		map((r: IHttpResponse<DeleteExpenseResponse>) => {
			if (!r.error) {
				const response: DeleteExpenseResponse = r.body!;
				return {
					deleteSuccess: true,
					balance: response.balance
				};
			} else
				return {
					deleteSuccess: false
				};
		})
	);
};

const updateExpenseService = (url: string, expense: Expense): Observable<UpdateExpenseResult> => {
	return HttpClient.put_r<UpdateExpenseResponse>(url, expense).pipe(
		map((r: IHttpResponse<UpdateExpenseResponse>) => {
			let result: UpdateExpenseResult = { updateSuccess: false };
			if (!r.error) {
				result = {
					updateSuccess: true,
					expense: r.body!.entity
				};
			} else {
				result = {
					updateSuccess: false
				};
			}

			return result;
		})
	);
};

const getGrouppedExpensesService = (
	url: string
): Observable<GrouppedExpensesResult> => {
	return HttpClient.get_r<GrouppedExpensesResult>(url).pipe(
		map((r: IHttpResponse<GrouppedExpensesResult>) => {
			const result: GrouppedExpensesResult = r.body!;
			return result;
		})
	);
};

export {
	getExpensesService,
	addNewExpenseService,
	getWalletBalanceService,
	deleteExpenseService,
	updateExpenseService,
	getGrouppedExpensesService
};
