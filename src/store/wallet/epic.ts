import { ofType, ActionsObservable } from "redux-observable";
import actions from "./actions";
import {
	WalletTopUpAction,
	WalletTopUpResponse,
	WalletTopUpRequest,
	GetExpensesAction,
	NewExpenseAction,
	Expense,
	GetWalletBalanceAction,
	ModifyExpenseAction,
	DeleteExpenseResult,
	UpdateExpenseResult,
	GrouppedExpensesResult
} from "./interfaces";
import { switchMap, map, catchError, mergeMap } from "rxjs/operators";
import { ajax, AjaxError } from "rxjs/ajax";
import { of, concat, Observable } from "rxjs";
import {
	getExpensesService,
	addNewExpenseService,
	getWalletBalanceService,
	deleteExpenseService,
	updateExpenseService,
	getGrouppedExpensesService
} from "./services";
import {
	getCurrentState,
	deleteEntityUrlMappings,
	replaceObjectInArray
} from "../../utils/generalUtils";
import { BaseEpicResult, ModifyActions } from "../interfaces";
import { Notification } from "rsuite";
import { convertLocaleMessage } from "../../locale/translator";

const BASE_URL = "http://localhost:4000/api/wallets";

const updateWalletBalance = (amount: number) => ({
	type: actions.TOP_UP_RESPONSE,
	amount
});

const GetWalletBalanceEpic = (
	action: ActionsObservable<GetWalletBalanceAction>
) =>
	action.pipe(
		ofType(actions.GET_WALLET_BALANCE_REQUEST),
		switchMap((action: GetWalletBalanceAction) => {
			return getWalletBalanceService(action.walletId).pipe(
				map((r: any) => {
					return {
						type: actions.GET_WALLET_BALANCE_RESPONSE,
						payload: { balance: r.payload.balance }
					};
				})
			);
		})
	);

const TopUpRequestEpic = (action: ActionsObservable<WalletTopUpAction>) =>
	action.pipe(
		ofType(actions.TOP_UP_REQUEST),
		switchMap((action: WalletTopUpAction): any => {
			const body: WalletTopUpRequest = {
				amount: action.topUpData.amount,
				reason: action.topUpData.message
			};

			return ajax
				.post(BASE_URL + `/${action.walletId}/topup`, body, {
					"Content-Type": "application/json"
				})
				.pipe(
					map(r => {
						const response: WalletTopUpResponse = r.response;
						let balance = 0;

						if (!response.error) {
							balance = response.updatedWallet.balance;
						}
						return updateWalletBalance(balance);
					})
				);
		}),
		catchError((er: AjaxError) => {
			console.log(er.response);
			return of(updateWalletBalance(0));
		})
	);

const GetExpensesEpic = (action: ActionsObservable<GetExpensesAction>) =>
	action.pipe(
		ofType(actions.GET_EXPENSES_REQUEST),
		switchMap((action: GetExpensesAction) => {
			return getExpensesService(action.walletId).pipe(
				map((result: any) => {
					let r: any = {};
					if (result.success) {
						r = {
								type: actions.GET_EXPENSES_RESPONSE_SUCCESS,
								payload: result.payload
						  }
					} else {
						Notification.error({
							title: "general.error",
							description: result.error
						})
						r = { type: actions.GET_EXPENSES_RESPONSE_ERROR };
					}

					return r;
				})
			);
		})
	);

const NewExpenseEpic = (action: ActionsObservable<NewExpenseAction>) =>
	action.pipe(
		ofType(actions.NEW_EXPENSE_REQUEST),
		mergeMap((action: NewExpenseAction) => {
			return addNewExpenseService(action.expense).pipe(
				map(
					(result: any): BaseEpicResult => {
						if (result.success) {
							const state = getCurrentState();
							const _expenses: Array<Expense> = [
								...state.wallet.latestExpenses,
								result.expense
							];

							return {
								type: actions.NEW_EXPENSE_RESPONSE_SUCCESS,
								payload: {
									balance: result.newBalance,
									latestExpenses: _expenses
								},
								extraPayload: { walletId: state.wallet.id }
							};
						} else {
							return {
								type: actions.NEW_EXPENSE_RESPONSE_ERROR,
								payload: {}
							};
						}
					}
				),
				mergeMap((newExpenseEpicResult: any) => {
					if (
						newExpenseEpicResult.type === actions.NEW_EXPENSE_RESPONSE_SUCCESS
					) {
						const walletId = newExpenseEpicResult.extraPayload.walletId;
						return concat(
							of(newExpenseEpicResult),
							of({
								type: actions.GET_WALLET_BALANCE_REQUEST,
								walletId
							})
						);
					} else {
						return of(newExpenseEpicResult);
					}
				})
			);
		})
	);

const ModifyExpenseEpic = (action: ActionsObservable<ModifyExpenseAction>) =>
	action.pipe(
		ofType(actions.MODIFY_EXPENSE_REQUEST),
		switchMap(
			(action: ModifyExpenseAction): Observable<BaseEpicResult> => {
				if (action.modificationAction === ModifyActions.DELETE) {
					const url = `${deleteEntityUrlMappings.expense}/${action.payload.id}`;

					return deleteExpenseService(url).pipe(
						map((deleteResult: DeleteExpenseResult) => {
							if (deleteResult.deleteSuccess) {
								const state = getCurrentState();
								const expenses: Array<Expense> = state.wallet.latestExpenses.filter(
									x => x.id !== action.payload.id
								);

								Notification.success({
									placement: "bottomEnd",
									title: convertLocaleMessage("general.success"),
									description: convertLocaleMessage("general.delete.success")
								})

								return {
									type: actions.MODIFY_EXPENSE_RESPONSE_SUCCESS,
									payload: { latestExpenses: expenses , balance: deleteResult.balance }
								};
							} else {
								Notification.error({
									placement: "bottomEnd",
									title: convertLocaleMessage("general.error"),
									description: convertLocaleMessage("delete.error")
								})
								return {
									type: actions.MODIFY_EXPENSE_RESPONSE_ERROR,
									payload: {}
								};
							}
						})
					);
				} else {
					const { id, data } = action.payload;
					const url = BASE_URL + `/expenses/${id}`;

					return updateExpenseService(url, data).pipe(
						map((result: UpdateExpenseResult) => {
							if (result.updateSuccess) {
								Notification.success({
									title: convertLocaleMessage("general.success"),
									description: convertLocaleMessage("general.update.success")
								});

								const state = getCurrentState();

								const expenses = replaceObjectInArray(
									state.wallet.latestExpenses,
									result.expense!
								);

								return {
									type: actions.MODIFY_EXPENSE_RESPONSE_SUCCESS,
									payload: { expenses }
								};
							} else {
								Notification.error({
									title: convertLocaleMessage("general.error"),
									description: convertLocaleMessage("general.update.error")
								});
								return {
									type: actions.MODIFY_EXPENSE_RESPONSE_ERROR
								};
							}
						})
					);
				}
			}
		)
	);

const GetGrouppedExpensesEpic = (
	action: ActionsObservable<GetExpensesAction>
) =>
	action.pipe(
		ofType(actions.GET_GROUPPED_EXPENSES_REQUEST),
		switchMap((action: GetExpensesAction) => {
			const url: string = BASE_URL + `/${action.walletId}/expenses/groupped`;

			return getGrouppedExpensesService(url).pipe(
				map((result: GrouppedExpensesResult) => ({
					type: actions.GET_GROUPPED_EXPENSES_RESPONSE,
					payload: {
						grouppedExpenses: result
					}
				}))
			);
		})
	);

const epics: any = [
	TopUpRequestEpic,
	GetExpensesEpic,
	NewExpenseEpic,
	GetWalletBalanceEpic,
	ModifyExpenseEpic,
	GetGrouppedExpensesEpic
];
export default epics;
