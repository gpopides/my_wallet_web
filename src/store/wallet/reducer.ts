import actions from "./actions"
import {Expense, GrouppedExpensesResult} from "./interfaces"

export interface IWalletState {
	balance: number;
	id: number | null;
	latestExpenses: Array<Expense>;
	grouppedExpenses: GrouppedExpensesResult;
	loadingCounter: number;
}

const initState: IWalletState = {
	balance: 0,
	id: null,
	grouppedExpenses: {},
	latestExpenses: [],
	loadingCounter: 0
}
 
const walletReducer = (state: IWalletState = initState, action: any) => {
	const reducedLoading = state.loadingCounter--;
	const incrementedLoading = state.loadingCounter++;

	switch (action.type) {
		case actions.TOP_UP_RESPONSE: 
			return {...state, balance: action.amount}
		case actions.GET_WALLET_RESPONSE_SUCCESS:
		case actions.GET_EXPENSES_RESPONSE_SUCCESS:
			return {...state, ...action.payload}

		case actions.NEW_EXPENSE_REQUEST:
			return {...state, loading: reducedLoading}

		case actions.NEW_EXPENSE_RESPONSE_SUCCESS:
		case actions.NEW_EXPENSE_RESPONSE_ERROR:
		case actions.GET_WALLET_BALANCE_RESPONSE:
		case actions.MODIFY_EXPENSE_RESPONSE_SUCCESS:
		case actions.GET_GROUPPED_EXPENSES_RESPONSE:
			return {...state, loading: incrementedLoading, ...action.payload}
		default:
			return state;
	}
}

export default walletReducer;
