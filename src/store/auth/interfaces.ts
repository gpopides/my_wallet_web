import actions from "./actions";
import { Wallet } from "../wallet/interfaces";


export interface LoginActionInterface {
	type: typeof actions.LOGIN_REQUEST
	username: string;
	password: string;
}


export interface LoginValues {
	username: string | null;
	password: string | null;
}

export interface User {
	id: string | null;
	username: string | null;

}

export interface LoginRequest {
	username: string;
	password: string;
}

export interface LoginResponse {
	token: string | null;
	user_id: string | null;
	error: string | null;
}
export interface LoginReponseResult {
	token: string | null;
	user: User | null
}


export interface IGetUserResponse {
	user: User,
	wallet: Wallet
}
export interface LoginResponseResult {
	type: typeof actions.LOGIN_RESPONSE_SUCCESS;
	payload: {
		token: string;
		user: User;
	}
}

export type AuthActions = LoginActionInterface;

