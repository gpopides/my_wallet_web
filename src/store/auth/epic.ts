import { ActionsObservable, ofType } from "redux-observable";
import actions from "./actions";
import { switchMap, catchError, mergeMap, map } from "rxjs/operators";
import { ajax, AjaxResponse, AjaxError } from "rxjs/ajax";
import {
	LoginActionInterface,
	LoginReponseResult,
	LoginRequest,
	LoginResponse,
	IGetUserResponse,
	LoginResponseResult
} from "./interfaces";
import { of, concat, Observable } from "rxjs";
import { Notification } from "rsuite";
import Translator from "../../locale/translator";
import generalActions from "../general/actions";
import walletActions from "../wallet/actions";
import { RequestHeaders } from "../interfaces";

const fetchUsers = (payload: any, type: string) => ({
	type,
	payload
});

const translator = new Translator("en_us");
const BASE_HEADERS: RequestHeaders = {
	"Content-Type": "application/json"
};

const loginRequestEpic = (action: ActionsObservable<LoginActionInterface>) =>
	action.pipe(
		ofType(actions.LOGIN_REQUEST),
		switchMap((action: LoginActionInterface) => {
			const body: LoginRequest = {
				username: action.username,
				password: action.password
			};
			return ajax
				.post("http://localhost:4000/auth/login", body, BASE_HEADERS)
				.pipe(
					map(r => {
						const response: LoginResponse = r.response;
						let payload: LoginReponseResult = {
							token: null,
							user: { id: null, username: null }
						};
						let notificationConfig;
						let actionType: string;

						if (!response.error) {
							payload = {
								token: response.token,
								user: {
									id: response.user_id,
									username: action.username
								}
							};
							notificationConfig = {
								title: translator.translate("login"),
								duration: 2000,
								description: translator.translate("login.success"),
								type: "success"
							};

							actionType = actions.LOGIN_RESPONSE_SUCCESS;
						} else {
							notificationConfig = {
								title: translator.translate("login"),
								duration: 2000,
								description: translator.translate("login.error"),
								type: "error"
							};
							actionType = actions.LOGIN_RESPONSE_ERROR;
						}
						(Notification as any)[notificationConfig.type](notificationConfig);

						return fetchUsers(payload, actionType);
					}),
					catchError((err: AjaxError): any => {
						console.log(err);
						Notification.error({
							title: "login",
							duration: 2000,
							description: "login failed check email password bla bla"
						});
						return of(fetchUsers({}, actions.LOGIN_RESPONSE_ERROR));
					})
				);
		})
	);

const getUserEpic = (action: ActionsObservable<any>) =>
	action.pipe(
		ofType(actions.LOGIN_RESPONSE_SUCCESS),
		switchMap((action: LoginResponseResult): any => {
			const { user, token } = action.payload;

			const headers: RequestHeaders = {
				"Authorization": `Bearer ${token}`
			}

			return ajax.get(`http://localhost:4000/api/users/${user.id}`, headers).pipe(
				mergeMap((r: AjaxResponse) => {
					const response: IGetUserResponse = r.response.result;
					const payload = response.wallet;

					const observables: Array<Observable<any>> = [
						of({
							type: walletActions.GET_WALLET_RESPONSE_SUCCESS,
							payload
						}),
						of({
							type: generalActions.REDIRECT,
							path: "home/overview"
						})
					];

					return concat(...observables);
				}),
				catchError((err: AjaxError) => {
					console.log(err);
					return of({
						type: walletActions.GET_WALLET_RESPONSE_ERROR
					});
				})
			);
		})
	);


const authEpics: any = [loginRequestEpic, getUserEpic];

export default authEpics;
