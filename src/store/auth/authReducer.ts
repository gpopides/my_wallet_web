import {User} from "./interfaces";
import actions from "./actions";


export interface IAuthState {
	token: string | null;
	user: User
}

const initState: IAuthState = {
	token: null,
	user: {
		id: null,
		username: null
	}
};

const authReducer = (state: IAuthState = initState, action: any) => {
	switch (action.type) {
		case actions.LOGIN_REQUEST: {
			return  {...state};
		}

		case actions.LOGIN_RESPONSE_SUCCESS: 
		case actions.LOGIN_RESPONSE_ERROR: {
			return  {...state, ...action.payload};
		}
		default:
			return state;
	}
};

export default authReducer;
