import {LoginValues} from "./interfaces";

const actions = {
	LOGIN_REQUEST: "LOGIN_REQUEST",
	LOGIN_RESPONSE_SUCCESS: "LOGIN_RESPONSE_SUCCESS",
	LOGIN_RESPONSE_ERROR: "LOGIN_RESPONSE_ERROR",

	GET_USER_RESPONSE_SUCCESS: "GET_USER_RESPONSE_ERROR",
	GET_USER_RESPONSE_ERROR: "GET_USER_RESPONSE_ERROR",

	loginRequest: (loginValues: LoginValues) => ({
		type: actions.LOGIN_REQUEST,
		username: loginValues.username,
		password: loginValues.password
	})
};

export default actions;
