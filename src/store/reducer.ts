import {combineReducers} from "redux";
import authReducer from "./auth/authReducer";
import walletReducer from "./wallet/reducer";
import {connectRouter} from "connected-react-router";
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web

const rootReducer = (history: any) => combineReducers({
	router: connectRouter(history),
	auth: authReducer, 
	wallet: walletReducer
});

interface IPersistConfig {
	key: string;
	storage: any;
}

const persistConfig: IPersistConfig = {
	key: 'root',
	storage
}

const persistedReducer = (history: any) => persistReducer(persistConfig, rootReducer(history));

export default persistedReducer;
