import {ActionsObservable, ofType} from "redux-observable";
import {map} from "rxjs/operators";
import actions from "./actions";
import { push } from "connected-react-router";

export interface IRedirect {
	path: string;
	params?: any;
}

const redirectEpic = (redirectAction: ActionsObservable<any>) => redirectAction.pipe(
	ofType(actions.REDIRECT),
	map((payload: IRedirect) => push(payload.path))
);

const epics = [redirectEpic];

export default epics;
