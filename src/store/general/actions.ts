import {IRedirect} from "./epic";

const actions = {
	REDIRECT: "REDIRECT",

	redirect: (path: string, params: IRedirect) => ({
		type: actions.REDIRECT,
		path,
		params
	})
}

export default actions;
