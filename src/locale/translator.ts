import EN_US from "./en_us";
import {LocaleMapping} from "./interfaces";

interface LocalesInterface {
	[key: string]: LocaleMapping
}

const locales: LocalesInterface = {"en_us": EN_US};

class Translator {
	locale: LocaleMapping;

	constructor(localeKey: string) {
		this.locale = locales[localeKey];
	}

	translate = (key: string) => {
		return this.locale[key] !== undefined ? this.locale[key] : "N/A";
	}
}

const t = new Translator("en_us");

export const convertLocaleMessage = (messageKey: string) => {
	return t.translate(messageKey);
}

export default Translator;
