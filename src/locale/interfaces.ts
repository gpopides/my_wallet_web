export interface LocaleMapping {
	[key: string]: string
}
