import {LocaleMapping} from "./interfaces";

const EN_US: LocaleMapping = {
	"login": "login",
	"login.success": "Success login",
	"login.error": "Login failed",

	"general.add": "Add",
	"general.close": "Close",
	"general.delete": "Delete",
	"general.edit": "Edit",
	"general.username": "Username",
	"general.password": "Password",
	"general.login": "Login",
	"general.date": "Date",
	"general.confirm": "Confirm",
	"general.success": "Success",
	"general.update.success": "Update succesful",
	"general.error": "Error",
	"general.update.error": "Update failed, try again",
	"general.delete.success": "Delete successful",
	"general.delete.error": "Delete failed, try again",


  "wallet.currentBalance": "Current Balance: ",
  "wallet.topUpMessage": "Top up",
	"wallet.expenses.sum": "Total count of expenses: ",
  "wallet.topUpReason": "Top up reason",

	"expenses.category": "Category",
	"expenses.amount": "Amount: ",
	"expenses.description": "Expense description: ",
	"expenses.new": "New expense",

	"expenses.categories.drink": "Drink",
	"expenses.categories.groceries": "Groceries",
	"expenses.categories.subscriptions": "Subscription",
	"expenses.categories.fastFood": "Fast food",
	"expenses.categories.other": "Other",

	"expenses.dateOfExpense": "Date of expense: ",

	"userDetails.wallet": "Wallet",
	"userDetails.expenses" : "Latest Expenses",
	"wallet.expenses.items": "Items",
	"wallet.expenses.add": "Add new expense",


	"error.usernameNotEmpty": "Username can not be empty",
	"error.passwordNotEmpty": "Password can not be empty",
	"errors.invalidAmount": "Amount must be more than 0",
	"error.expenses.invalidBody": "Invalid expense body",


	"home.sidebar.items.home": "Home",
	"home.sidebar.items.overview": "Expenses overview",


	"expensesOverview.filter.week": "By Week",
	"expensesOverview.filter.day": "By Day",
	"expensesOverview.filter.month": "By Month",


	"delete.error": "Delete failed"
};

export default EN_US;
