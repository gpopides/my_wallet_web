import React from "react";
import { Panel } from "rsuite";
import { connect } from "react-redux";
import { IState } from "../../store/interfaces";
import UserWallet from "../../Components/User/Wallet/wallet";

interface IOwnProps {
	style?: React.CSSProperties;
}

interface IStateProps {
	username?: string | null;
	balance?: number;
}

interface UserDetailsProps extends IOwnProps, IStateProps {}

const UserDetails: React.FunctionComponent<UserDetailsProps> = (
	props: UserDetailsProps
) => {
	return (
		<Panel bordered style={props.style}>
			<UserWallet />
		</Panel>
	);
};

UserDetails.defaultProps = {
	style: {}
};

const mapStateToProps = (state: IState): IStateProps => {
	return {
		username: state.auth.user.username,
		balance: state.wallet.balance
	};
};

const connectedUserDetails = connect(mapStateToProps, null)(UserDetails);

export default connectedUserDetails;
