import React from "react";
import { connect } from "react-redux";
import { IState } from "../../store/interfaces";
import { FlexboxGrid } from "rsuite";
import HomeSidebar from "../../Components/Home/HomeSidebar";
import Router from "../../Components/router/router";
import authorizedRoutes from "../../Components/router/authorizedRoutes";

interface ICompProps {

}

interface IStateProps {
}

interface HomeProps extends ICompProps, IStateProps {};

const Home: React.FunctionComponent<HomeProps> = (props: HomeProps) => {
	return (
		<FlexboxGrid justify={"start"} style={{marginTop: "10px"}}>
			<FlexboxGrid.Item colspan={4}>
				<HomeSidebar />
			</FlexboxGrid.Item>
			<FlexboxGrid.Item colspan={20}>
				<Router routes={authorizedRoutes} />
			</FlexboxGrid.Item>
		</FlexboxGrid>
	);
};

const mapStateToProps = (state: IState): IStateProps => {
	return {
	};
};

const mapActionsToProps = {};

const connectedHome = connect(mapStateToProps, mapActionsToProps)(Home);
export default connectedHome;
