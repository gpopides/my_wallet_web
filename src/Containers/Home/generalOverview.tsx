import React from "react";
import {FlexboxGrid} from "rsuite";
import UserDetails from "../User/UserDetails";
import Expenses from "../../Components/User/Wallet/Expenses/expenses";

const GeneralOverview: React.FC = (props: any) => {
	return (
		<FlexboxGrid justify={"start"}>
			<FlexboxGrid.Item colspan={12}>
				<UserDetails style={{width: "80%", marginLeft: "20px"}}/>
			</FlexboxGrid.Item>
			<FlexboxGrid.Item colspan={12} justify={"center"}>
				<Expenses style={{marginRight: "20px"}}/>
			</FlexboxGrid.Item>
		</FlexboxGrid>
	)
}

export default GeneralOverview;
