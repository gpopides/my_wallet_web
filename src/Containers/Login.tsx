import React, { useState, useRef } from "react";
import {
	FlexboxGrid,
	Button,
	Panel,
	Form,
	Schema,
	FormGroup,
	ButtonToolbar
} from "rsuite";
import { connect } from "react-redux";
import authActions from "../store/auth/actions";
import { LoginValues } from "../store/auth/interfaces";
import TranslatorC from "../Components/general/translator";
import { convertLocaleMessage } from "../locale/translator";
import { TextField, InputTypes } from "../Components/general/formUtils";
const { loginRequest } = authActions;

interface LoginProps {
	loginRequest(loginValues: LoginValues): void;
}

const initLoginValues: LoginValues = { username: null, password: null };

const Login: React.FC<LoginProps> = (props: LoginProps) => {
	const formModel = Schema.Model({
		username: Schema.Types.StringType().isRequired(
			convertLocaleMessage("error.usernameNotEmpty")
		),
		password: Schema.Types.StringType().isRequired(
			convertLocaleMessage("error.usernameNotEmpty")
		)
	});

	const [formValues, updateFormValues] = useState<LoginValues>(initLoginValues);
	const form: any = useRef<HTMLElement>(null);

	const handleSubmit = () => {
		if (form.current.check()) {
			props.loginRequest(formValues);
		}
	};

	const handleFormChange = (v: any) => {
		updateFormValues(v);
	};

	return (
		<FlexboxGrid justify={"center"} style={{ marginTop: "10px" }}>
			<FlexboxGrid.Item>
				<Panel bordered style={{ width: "500px" }}>
					<Form
						model={formModel}
						formValue={formValues}
						fluid
						onChange={handleFormChange}
						ref={form}
					>
						<TextField
							name={"username"}
							label={<TranslatorC localeKeyId={"general.username"} />}
						/>
						<TextField
							name={"password"}
							label={<TranslatorC localeKeyId={"general.password"} />}
							type={InputTypes.password}
						/>

						<FormGroup>
							<ButtonToolbar>
								<Button appearance={"primary"} onClick={handleSubmit}>
									<TranslatorC localeKeyId={"general.login"} />
								</Button>
							</ButtonToolbar>
						</FormGroup>
					</Form>
				</Panel>
			</FlexboxGrid.Item>
		</FlexboxGrid>
	);
};

const mapStateToProps = (state: any) => {
	return {};
};

const mapActionsToProps = {
	loginRequest
};

const connectedLogin = connect<any, any, any>(
	mapStateToProps,
	mapActionsToProps
)(Login);
export default connectedLogin;
