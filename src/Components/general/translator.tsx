import React from "react";
import Translator from "../../locale/translator";


interface ITranslatorProps {
	localeKeyId: string;
	children?: any;
}

const _translator = new Translator("en_us");

const TranslatorC: React.FC<ITranslatorProps> = (props: ITranslatorProps) => {
	const msg = (): string => {
			return _translator.translate(props.localeKeyId) + " ";
	}
	return (
		<React.Fragment>
			{msg()}
			{props.children}
		</React.Fragment>
	)
}

export default TranslatorC;
