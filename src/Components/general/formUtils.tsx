import React from "react";
import { FormGroup, ControlLabel, FormControl, HelpBlock } from "rsuite";

export enum InputTypes {
	email = "email",
	password = "password"
}

enum FormColors {
	ERROR = "red"
}

interface ITextField {
	label: string | JSX.Element;
	name: string;
	type?: InputTypes;
}

interface IErrorBlock {
	message: string;
	active: boolean;
}

const TextField: React.FC<ITextField> = (props: ITextField) => {
	const { label, name, ...rest } = props;

	return (
		<FormGroup>
			<ControlLabel>{label}</ControlLabel>
			<FormControl name={name} {...rest} />
		</FormGroup>
	);
};

const ErrorBlock: React.FC<IErrorBlock> = (props: IErrorBlock) => {

	return (
		<React.Fragment>
			{props.active && (
				<HelpBlock style={{ color: FormColors.ERROR }}>
					{props.message}
				</HelpBlock>
			)}
		</React.Fragment>
	);
};

export { TextField, ErrorBlock };
