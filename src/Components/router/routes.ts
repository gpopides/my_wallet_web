import { FunctionComponent } from "react";
import Login from "../../Containers/Login";
import authorizedRoutes from "./authorizedRoutes";
import Home from "../../Containers/Home/home";

export interface IRoute {
	component: FunctionComponent
	exact: boolean;
	path: string;
	children?: Array<IRoute>
}

const routes: Array<IRoute> = [
	{
		component: Home,
		path: "/home",
		exact: false,
		children: authorizedRoutes
	},
	{
		component: Login,
		path: "/",
		exact: true
	}
]

export default routes;
