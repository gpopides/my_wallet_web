import React from "react";
import { Route } from "react-router-dom";
import { IRoute } from "./routes";
import {history} from "../../store/store";
import {ConnectedRouter} from "connected-react-router";

type RouterProps = {
	routes: Array<IRoute>
}

const Router: React.FC<RouterProps> = (props: RouterProps): any => {
	const buildRoutes = () => {
		return props.routes.map((r: IRoute) => {
			return (
				<Route
					component={r.component}
					exact={r.exact}
					path={r.path}
					key={r.path}
				/>
			);
		});
	};
	return <ConnectedRouter history={history}>{buildRoutes()}</ConnectedRouter>;
};

export default Router;
