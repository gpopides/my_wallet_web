import { IRoute } from "./routes";
import ExpenseOverview from "../User/Wallet/Expenses/ExpenseOverview";
import GeneralOverview from "../../Containers/Home/generalOverview";

const authorizedRoutes : Array<IRoute> = [
	{
		component: ExpenseOverview,
		path: "/home/expensesOverview",
		exact: true 
	},
	{
		component: GeneralOverview,
		path: "/home/overview",
		exact: true 
	}
]

export default authorizedRoutes;
