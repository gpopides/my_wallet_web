export type InvalidString = string | null | undefined;

export interface IInvalidField{
	[key: string]: boolean;
}

export interface IValidationResult {
	valid: boolean;
	errs: IInvalidField;
}
