import React, { useState } from "react";
import { Avatar, Button, Grid, Row, Col, Input, InputNumber } from "rsuite";
import { convertLocaleMessage } from "../../../locale/translator";
import TranslatorC from "../../general/translator";
import { InvalidString, IInvalidField} from "../../interfaces";
import { StringUtils } from "../../../utils/typeUtils";
import { ErrorBlock } from "../../general/formUtils";
import { ITopUpData } from "../../../store/wallet/interfaces";
import { IState } from "../../../store/interfaces";
import actions from "../../../store/wallet/actions";
import { connect } from "react-redux";

const { topUpRequest } = actions;

const initTopUpData = { amount: 0, message: "" };

const initErrors: IInvalidField = { message: false, amount: false };

const UserWallet: React.FunctionComponent<IUserWalletProps> = (
	props: IUserWalletProps
) => {
	const [topUpAreaIsvisible, updateTopUpAreaVisibilty] = useState<boolean>(
		false
	);
	const [topUpData, updateTopUpData] = useState<ITopUpData>(initTopUpData);
	const [errors, updateError] = useState<IInvalidField>(initErrors);

	const handleTopUpDataChange = (key: string) => (value: any) => {
		updateTopUpData(ps => ({
			...ps,
			[key]: value
		}));
	};

	const handleTopUpTrigger = () => {
		updateTopUpAreaVisibilty(prevVisibility => !prevVisibility);
	};

	const getTopUpButtonLabel = () => {
		const key: string = !topUpAreaIsvisible
			? "wallet.topUpMessage"
			: "general.close";
		return <TranslatorC localeKeyId={key} />;
	};

	const validate = async (data: ITopUpData): Promise<boolean> => {
		return new Promise((resolve: any) => {
			const validationObj: IInvalidField = {
				message: StringUtils.stringIsEmpty(data.message),
				amount: data.amount === null || data.amount === 0
			};

			const invalidObjs = Object.values(validationObj).filter(
				(v: boolean) => v
			);

			if (!invalidObjs.length) {
				updateError(initErrors);
				resolve(initErrors);
			} else {
				updateError(validationObj);
				resolve(false);
			}
		});
	};

	const topUp = async () => {
		await validate(topUpData).then((valid: boolean) => {
			if (valid) {
				props.topUpRequest(topUpData, props.walletId);
			}
		});
	};

	const getTopUpArea = () => {
		let area = null;
		if (topUpAreaIsvisible) {
			area = (
				<Row style={{ marginTop: "5px" }}>
					<Col lg={16}>
						<Input
							value={topUpData.message}
							onChange={handleTopUpDataChange("message")}
							placeholder={convertLocaleMessage("wallet.topUpReason")}
						/>
						<ErrorBlock message={"invalid message"} active={errors.message} />
					</Col>
					<Col lg={4}>
						<InputNumber
							value={topUpData.amount}
							onChange={handleTopUpDataChange("amount")}
							min={0}
						/>
						<ErrorBlock message={"invalid amount"} active={errors.amount} />
					</Col>
					<Col lg={4} onClick={topUp}>
						<Button>
							<TranslatorC localeKeyId={"general.add"} />
						</Button>
					</Col>
				</Row>
			);
		}

		return area;
	};

	const constructAvatar = (username: InvalidString) => {
		const getAvatarWidth = (username: InvalidString) => {
			return username ? username.length * 12 : 50;
		};

		return username ? (
			<Avatar
				style={{
					color: "#7B1FA2",
					borderRadius: 12,
					width: getAvatarWidth(username)
				}}
			>
				{props.username}
			</Avatar>
		) : null;
	};

	const avatar = constructAvatar(props.username);

	return (
		<Grid fluid>
			<Row>
				<Col lg={5}>{avatar}</Col>
				<Col lg={7}>
					<TranslatorC localeKeyId={"wallet.currentBalance"} /> {props.balance}
				</Col>
				<Col lg={4}>
					<Button onClick={handleTopUpTrigger}>{getTopUpButtonLabel()}</Button>
				</Col>
			</Row>
			{getTopUpArea()}
		</Grid>
	);
};

const mapActionsToProps = {
	topUpRequest
};

const mapStateToProps = (state: IState) => {
	return {
		balance: state.wallet.balance,
		username: state.auth.user.username,
		walletId: state.wallet.id
	};
};

interface IDispatchProps {
	topUpRequest(topUpData: ITopUpData, walletId: number | null): any;
}

interface IOwnProps {}

interface IStateProps {
	balance: any;
	username?: string | null;
	walletId: number | null;
}

type IUserWalletProps = IDispatchProps & IStateProps & IOwnProps;

const connected = connect<IStateProps, IDispatchProps, IOwnProps, IState>(
	mapStateToProps,
	mapActionsToProps
)(UserWallet);
export default connected;
