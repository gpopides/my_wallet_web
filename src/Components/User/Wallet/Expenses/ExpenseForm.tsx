import React, {  useEffect } from "react";
import {
	Button,
	Input,
	FlexboxGrid,
	InputNumber,
	SelectPicker,
	DatePicker,
	Icon,
	Alert
} from "rsuite";
import { connect } from "react-redux";
import { IState } from "../../../../store/interfaces";
import { Expense } from "../../../../store/wallet/interfaces";
import { DEFAULT_DATE, DATE_FORMAT } from "../../../../utils/generalUtils";
import { convertLocaleMessage } from "../../../../locale/translator";
import { useFormFields } from "../../../../utils/customHooks";
import TranslatorC from "../../../general/translator";
import moment from "moment";
import {objectIsEmpty} from "../../../../utils/typeUtils";

type DataItemType = {
	value: string;
	label: string | JSX.Element;
	children?: Array<DataItemType>;
	groupBy?: string;
};

const initExpense: Expense = {
	date: DEFAULT_DATE,
	description: "",
	amount: 0,
	category: "",
	walletId: 0
};


const ExpenseForm: React.FC<ExpenseFormProps> = (
	props: ExpenseFormProps
) => {
	const [newExpense, updateExpense] = useFormFields<Expense>(initExpense);

	useEffect(() => {
		if(!objectIsEmpty(props.expense)) {
			updateExpense(true, props.expense);
		} else {
			updateExpense(false, null, "walletId", props.walletId);
		}
	}, []);


	const handleExpenseChange = (field: string) => (
		value: string | number
	) => {
		updateExpense(false, null, field, value);
	};

	const handleExpenseDateChange = (newDate: Date) => {
		const _date: string = moment(newDate).format(DATE_FORMAT);
		updateExpense(false, null, "date", _date);
	};

	const confrimExpense = () => {
		if (newExpense.amount > 0) {
			props.handleSubmit(newExpense);
		} else {
			Alert.error(convertLocaleMessage("errors.invalidAmount"));
		}
	};

	const createSelectPickerItemLabel = (value: string) => {
		const key: string = `expenses.categories.${value}`;
		return (
			<React.Fragment>
				<TranslatorC localeKeyId={key} />
			</React.Fragment>
		);
	};

	const selectPickerData: Array<DataItemType> = [
		{
			value: "groceries",
			label: createSelectPickerItemLabel("groceries")
		},
		{
			value: "drink",
			label: createSelectPickerItemLabel("drink")
		},
		{
			value: "subscriptions",
			label: createSelectPickerItemLabel("subscriptions")
		},
		{
			value: "fastFood",
			label: createSelectPickerItemLabel("fastFood")
		},
		{
			value: "other",
			label: createSelectPickerItemLabel("other")
		}
	];

	return (
			<FlexboxGrid justify={"start"} >
			<FlexboxGrid.Item colspan={6}>
				<SelectPicker
					data={selectPickerData}
					placeholder={convertLocaleMessage("expenses.category")}
					value={newExpense.category}
					onChange={handleExpenseChange("category")}
					style={{ width: "250px" }}
					searchable={false}
				/>
			</FlexboxGrid.Item>
			<FlexboxGrid.Item colspan={2}>
				<TranslatorC localeKeyId={"expenses.amount"} />
			</FlexboxGrid.Item>
			<FlexboxGrid.Item colspan={4}>
				<InputNumber
					value={newExpense.amount}
					onChange={handleExpenseChange("amount")}
					style={{ width: "150px" }}
					min={0}
					postfix={<Icon icon={"eur"} />}
				/>
				
			</FlexboxGrid.Item>
			<FlexboxGrid.Item colspan={6}>
				<Input
					value={newExpense.description}
					onChange={handleExpenseChange("description")}
					style={{ width: "250px" }}
					placeholder={convertLocaleMessage("expenses.description")}
				/>
			</FlexboxGrid.Item>
			<FlexboxGrid.Item colspan={4}>
				<DatePicker
					style={{ width: "250px" }}
					defaultValue={moment().toDate()}
					onChange={handleExpenseDateChange}
				/>
			</FlexboxGrid.Item>
			<FlexboxGrid.Item colspan={2}>
				<Button onClick={confrimExpense} style={{marginLeft: "10px"}} color={'green'}>
					<TranslatorC localeKeyId={'general.confirm'} />
				</Button>
			</FlexboxGrid.Item>
		</FlexboxGrid>
	);
};

interface IOwnProps {
	walletId: number | null;
	expense?: Expense;
	handleSubmit: any;
}
interface IStateProps {}
interface IActionProps {
}

type ExpenseFormProps = IOwnProps & IStateProps & IActionProps;

const mapStateToProps = (state: IState): IStateProps => {
	return {};
};

const mapActionsToProps: IActionProps = {
};

export default connect<IStateProps, IActionProps, IOwnProps, IState>(
	mapStateToProps,
	mapActionsToProps
)(ExpenseForm);
