import React from "react";
import {List} from "rsuite";
import {Expense} from "../../../../store/wallet/interfaces";
import ExpensesListItem from "./ExpensesListItem";

interface ExpensesListProps {
	expenses: Array<Expense>
}

const ExpensesList: React.FC<ExpensesListProps> = (props: ExpensesListProps) => {
	const listItemsFactory = () => {
		return props.expenses.map((expense: Expense, i: number) => {
			return <List.Item key={i} >
				<ExpensesListItem expense={expense} />
			</List.Item>
		});
	}

	const listItems =listItemsFactory();
	return (
		<List bordered style={{width: "100%"}}>
			{listItems}
		</List>
	)
}

export default ExpensesList;
