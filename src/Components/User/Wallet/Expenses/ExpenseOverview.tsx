import React, { useEffect, ReactNode, useState, ReactElement } from "react";
import { SelectPicker, Row, Col, Grid, Divider } from "rsuite";
import TranslatorC from "../../../general/translator";
import ExpensesFilteredByDay from "./ExpensesFilteredByDay";
import ExpensesFilteredByWeek from "./ExpensesFilteredByWeek";
import ExpensesFilteredByMonth from "./ExpensesFilteredByMonth";
import { IState } from "../../../../store/interfaces";
import walletActions from "../../../../store/wallet/actions";
import { connect } from "react-redux";

const { fetchGrouppedExpenses } = walletActions;

type SelectItem = {
	value: string;
	label: ReactNode;
};

const initFilter = "day";

const FilterComponents: { [key: string]: ReactElement } = {
	day: <ExpensesFilteredByDay />,
	week: <ExpensesFilteredByWeek />,
	month: <ExpensesFilteredByMonth />
};

const ExpenseOverview: React.FC<ExpenseOverviewProps> = (props: ExpenseOverviewProps) => {
	const [selectedFilterKey, changeSelectedFilterKey] = useState<string>(
		initFilter
	);

	useEffect(() => {
		if (props.walletId) {
			props.fetchGrouppedExpenses(props.walletId)
		}
	}, [props, props.walletId])

	const selectData: Array<SelectItem> = [
		{
			value: "day",
			label: <TranslatorC localeKeyId={"expensesOverview.filter.day"} />
		},
		{
			value: "week",
			label: <TranslatorC localeKeyId={"expensesOverview.filter.week"} />
		},
		{
			value: "month",
			label: <TranslatorC localeKeyId={"expensesOverview.filter.month"} />
		}
	];

	const getFilterComponent = (key: string): ReactElement => {
		return FilterComponents[key];
	};

	return (
		<Grid fluid>
			<Row>
				<Col lg={24}>
					<SelectPicker
						data={selectData}
						searchable={false}
						onSelect={changeSelectedFilterKey}
						value={selectedFilterKey}
						cleanable={false}
					/>
				</Col>
			</Row>
			<Divider />
			<Row>
				<Col lg={24}>{getFilterComponent(selectedFilterKey)}</Col>
			</Row>
		</Grid>
	);
};

interface IOwnProps {}
interface IStateProps {
	walletId: number | null;
}

interface IActionProps {
	fetchGrouppedExpenses(walletId: number): void;
}

const mapActionsToProps: IActionProps = {
	fetchGrouppedExpenses 
};

const mapStateToProps = (state: IState): IStateProps => {
	return {
		walletId: state.wallet.id
	}
};

type ExpenseOverviewProps = IActionProps & IStateProps & IOwnProps;

export default connect<IStateProps, IActionProps, IOwnProps, IState>(
	mapStateToProps,
	mapActionsToProps
)(ExpenseOverview);
