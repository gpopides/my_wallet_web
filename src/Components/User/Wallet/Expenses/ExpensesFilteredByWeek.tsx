import React from "react";
import { connect } from "react-redux";
import { IState } from "../../../../store/interfaces";
import {getFilteredExpenses, ExpenseFilter} from "../../../../store/wallet/selectors";
import ExpensesList from "./ExpensesList";
import {Grid, Row, Col} from "rsuite";

const ExpensesFilteredByWeek: React.FC<ExpensesFilteredByWeekProps> = (props: ExpensesFilteredByWeekProps) => {
	return (
		<Grid fluid>
			<Row >
				<Col lg={12}>
					<ExpensesList expenses={props.expenses} />
				</Col>
			</Row>
		</Grid>
	)
};

interface IOwnProps {}
interface IStateProps {
	expenses: any;
}

interface IActionProps {}

const mapActionsToProps: IActionProps = {};

const mapStateToProps = (state: IState): IStateProps => {
	return {
		expenses: getFilteredExpenses(state, ExpenseFilter.WEEK)
	};
};
type ExpensesFilteredByWeekProps = IActionProps & IStateProps & IOwnProps;

export default connect<IStateProps, IActionProps, IOwnProps, IState>(
	mapStateToProps,
	mapActionsToProps
)(ExpensesFilteredByWeek);
