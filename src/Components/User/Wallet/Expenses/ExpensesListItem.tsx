import React from "react";
import { Expense } from "../../../../store/wallet/interfaces";
import { getTagColor } from "../../../../utils/generalUtils";
import { Icon, Tag, Row, Grid, Col, Alert } from "rsuite";
import TranslatorC from "../../../general/translator";
import { connect } from "react-redux";
import {
	IState,
	ModifyAction,
	ModifyEntityPayload,
	ModifyActions
} from "../../../../store/interfaces";
import walletActions from "../../../../store/wallet/actions";
import { useBoolean } from "../../../../utils/customHooks";
import ExpenseForm from "./ExpenseForm";
import {convertLocaleMessage} from "../../../../locale/translator";

const { modifyExpense } = walletActions;

const ExpensesListItem: React.FC<ExpensesListItemProps> = (
	props: ExpensesListItemProps
) => {
	const [isEditMode, updateIsEditMode] = useBoolean(false);

	const getLocalizedDescription = (category: string) => {
		const key: string = `expenses.categories.${category}`;
		return <TranslatorC localeKeyId={key} />;
	};

	const deleteExpense = (expenseId?: number) => () => {
		if (expenseId) {
			props.modifyExpense(ModifyActions.DELETE, { id: expenseId });
		}
	};

	const editExpense = (expense: Expense) => {
		updateIsEditMode();
		const {id, ...data} = expense;
		if (id) {
			props.modifyExpense(ModifyActions.EDIT, {id, data});
		} else {
			Alert.error(convertLocaleMessage("error.expenses.invalidBody"))
		}
	}

	const getDisplayItem = (): JSX.Element => {
		let display;
		if (!isEditMode) {
			display = (
				<Row gutter={2}>
					<Col lg={4}>
						<TranslatorC
							localeKeyId={"expenses.amount"}
							children={props.expense.amount}
						/>
						<Icon icon={"eur"} />
					</Col>
					<Col lg={5}>
						<TranslatorC localeKeyId={"expenses.description"}>
							{props.expense.description}
						</TranslatorC>
					</Col>
					<Col lg={5}>
						<TranslatorC localeKeyId={"expenses.dateOfExpense"} />
						{props.expense.date}
					</Col>
					<Col lg={2}>
						<Icon icon={"edit"} onClick={updateIsEditMode} />
					</Col>
					<Col lg={2}>
						<Icon icon={"trash"} onClick={deleteExpense(props.expense.id)} />
					</Col>
					<Col lg={2}>
						<Tag color={getTagColor(props.expense.category)}>
							{getLocalizedDescription(props.expense.category!)}
						</Tag>
					</Col>
				</Row>
			);
		} else {
			display = (
				<Grid fluid>
					<Row>
						<Col lg={24}>
							<ExpenseForm
								walletId={props.walletId}
								expense={props.expense}
								handleSubmit={editExpense}
							/>
						</Col>
					</Row>
				</Grid>
			);
		}

		return display;
	};

	return <React.Fragment>{getDisplayItem()}</React.Fragment>;
};

interface IOwnProps {
	expense: Expense;
}

interface IActionProps {
	modifyExpense(action: ModifyAction, payload: ModifyEntityPayload): void;
}

interface IStateProps {
	walletId: number | null;
}

const mapActionsToProps: IActionProps = {
	modifyExpense,
};

const mapStateToProps = (state: IState): IStateProps => {
	return {
		walletId: state.wallet.id
	};
};

type ExpensesListItemProps = IOwnProps & IActionProps & IStateProps;

export default connect<IStateProps, IActionProps, IOwnProps, IState>(
	mapStateToProps,
	mapActionsToProps
)(ExpensesListItem);
