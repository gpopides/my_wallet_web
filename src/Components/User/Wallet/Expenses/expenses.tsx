import React, { useEffect } from "react";
import { connect } from "react-redux";
import { IState } from "../../../../store/interfaces";
import { Expense } from "../../../../store/wallet/interfaces";
import {  Panel, Button, Divider, Grid, Col } from "rsuite";
import ExpensesList from "./ExpensesList";
import TranslatorC from "../../../general/translator";
import actions from "../../../../store/wallet/actions";
import ExpenseForm from "./ExpenseForm";
import { useBoolean } from "../../../../utils/customHooks";
import {sumCollection} from "../../../../utils/collectionUtils";
import {getLatestExpenses} from "../../../../store/wallet/selectors";
const { submitNewExpense, fetchExpenses } = actions;

const Expenses: React.FC<ExpensesProps> = (props: ExpensesProps) => {
	const [newExpenseAreaVisible, updateNewExpenseAreaVisibility] = useBoolean(false);

	useEffect(() => {
		if (props.walletId) {
			props.fetchExpenses(props.walletId);
		}
	}, []);

	const handleNewExpenseSubmit = (expense: Expense) => {
		props.submitNewExpense(expense);
		updateNewExpenseAreaVisibility();
	}

	const calculateLatestExpensesTotalSum = (expenses: Array<Expense>) => {
		return sumCollection(expenses, "amount");
	}

	const constructHeader = () => {
		const buttonLocaleKey = !newExpenseAreaVisible
			? "wallet.expenses.add"
			: "general.close";

		return (
			<Grid fluid>
				<Col lg={3}>
					<TranslatorC localeKeyId={"userDetails.expenses"} />{" "}
				</Col>
				<Col lg={18}>
					{calculateLatestExpensesTotalSum(props.expenses)}
				</Col>
				<Col lg={3}>
					<Button
						type={"primary"}
						color={"cyan"}
						onClick={updateNewExpenseAreaVisibility}
					>
						<TranslatorC localeKeyId={buttonLocaleKey} />
					</Button>
				</Col>
			</Grid>
		);
	};

	const _style = props.style !== undefined ? props.style : {};

	return (
		<Panel style={{ ..._style }} bordered header={constructHeader()}>
			{newExpenseAreaVisible && <ExpenseForm walletId={props.walletId} handleSubmit={handleNewExpenseSubmit}/>}
			{props.expenses.length > 0 && <Divider />}
			{props.expenses.length > 0 && <ExpensesList expenses={props.expenses} />}
		</Panel>
	);
};

interface IOwnProps {
	style?: React.CSSProperties;
}
interface IStateProps {
	expenses: Array<Expense>;
	walletId: number | null;
}

interface IActionProps {
	fetchExpenses(walletId: number): any;
	submitNewExpense(expense: Expense): void;
}

const mapActionsToProps: IActionProps = {
	fetchExpenses,
	submitNewExpense
};

const mapStateToProps = (state: IState): IStateProps => {
	return {
		expenses: getLatestExpenses(state),
		walletId: state.wallet.id
	};
};

type ExpensesProps = IOwnProps & IStateProps & IActionProps;

export default connect<IStateProps, IActionProps, IOwnProps, IState>(
	mapStateToProps,
	mapActionsToProps
)(Expenses);
