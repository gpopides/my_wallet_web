import React from "react";
import { connect } from "react-redux";
import { IState } from "../../../../store/interfaces";
import {getFilteredExpenses, ExpenseFilter} from "../../../../store/wallet/selectors";
import ExpensesList from "./ExpensesList";
import {Grid, Row, Col} from "rsuite";

const ExpensesFilteredByMonth: React.FC<ExpensesFilteredByMonthProps> = (props: ExpensesFilteredByMonthProps) => {
	return (
		<Grid fluid>
			<Row >
				<Col lg={12}>
					<ExpensesList expenses={props.expenses} />
				</Col>
			</Row>
		</Grid>
	)
};

interface IOwnProps {}
interface IStateProps {
	expenses: any;
}

interface IActionProps {}

const mapActionsToProps: IActionProps = {};

const mapStateToProps = (state: IState): IStateProps => {
	return {
		expenses: getFilteredExpenses(state, ExpenseFilter.MONTH)
	};
};
type ExpensesFilteredByMonthProps = IActionProps & IStateProps & IOwnProps;

export default connect<IStateProps, IActionProps, IOwnProps, IState>(
	mapStateToProps,
	mapActionsToProps
)(ExpensesFilteredByMonth);
