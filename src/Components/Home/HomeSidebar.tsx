import React, {useState} from "react";
import {IState} from "../../store/interfaces";
import { Nav, Icon, Sidenav} from "rsuite";
import {connect} from "react-redux";
import {IconNames} from "rsuite/lib/Icon";
import TranslatorC from "../general/translator";
import generalActions from "../../store/general/actions";
import {IRedirect} from "../../store/general/epic";

const {redirect} = generalActions;

type NavbarItem = {
	key: string;
	iconName: IconNames;
}

const paths: {[key: string]: string} = {
	home: "/home/overview",
	overview: "/home/expensesOverview",
}

const HomeSidebar: React.FC<HomeSidebarProps> = (props: HomeSidebarProps) => {
	const [activeKey, changeActiveKey] = useState<string>("home");


	const buildSidebarItems = (): Array<JSX.Element> => {
		const getItems = (): Array<NavbarItem> => {
			return [
				{
					key: "home",
					iconName: "home",
				},
				{
					key: "overview",
					iconName: "history",
				}
			];
		}

		return getItems().map((item: NavbarItem) => {
			return <Nav.Item eventKey={item.key} icon={<Icon icon={item.iconName} />} onSelect={handleMenuSelect}>
				<TranslatorC localeKeyId={`home.sidebar.items.${item.key}`} />
			</Nav.Item>
		})
	}

	const handleMenuSelect = (selectedKey: string) => {
		changeActiveKey(selectedKey);
		props.redirect(paths[selectedKey]);
	}

	const sidebarItems = buildSidebarItems();

	return (
		<div style={{width: "250px"}}>
		<Sidenav activeKey={activeKey} onSelect={handleMenuSelect}>
			<Sidenav.Body>
				<Nav >
					{sidebarItems}
				</Nav>
			</Sidenav.Body>
		</Sidenav>
		</div>
	)
}

interface IOwnProps {}
interface IStateProps {}
interface IActionProps {
	redirect(path:string, params?: IRedirect): any;
}

const mapActionsToProps: IActionProps = {
	redirect
}

const mapStateToProps = (state: IState) : IStateProps => {
	return {
	}
}

type HomeSidebarProps = IOwnProps & IActionProps & IStateProps;

export default connect<IStateProps, IActionProps, IOwnProps, IState>(mapStateToProps, mapActionsToProps)(HomeSidebar);
