import {store} from "./store/store";
import {ajax, AjaxResponse, AjaxError} from "rxjs/ajax";
import {RequestHeaders} from "./store/interfaces";
import {switchMap, catchError} from "rxjs/operators";
import {of, Observable} from "rxjs";
import {IAuthState} from "./store/auth/authReducer";

export type IHttpResponse<T> = {
	body?: T;
	status: number;
	error: boolean;
	errorMsg?: string;
}


class HttpClient {
	static fetchToken = (): string | null => {
		const authState: IAuthState = store.getState().auth;
		let _token: string | null = null;

		if (authState.token) {
			_token = authState.token;
		}
		return _token;
	}


	static getHeaders = (): RequestHeaders => {
		const token: string | null= HttpClient.fetchToken();

		return {
			"Content-Type": "application/json",
			"Authorization": `Bearer ${token}`
		};
	}

	static get_r = <T> (url: string, headers?: RequestHeaders): Observable<IHttpResponse<T>> => {
		const defaultHeaders: RequestHeaders = HttpClient.getHeaders();
		const _headers = {...defaultHeaders, ...headers};

		return ajax.get(url, _headers).pipe(
			switchMap((r: AjaxResponse): Observable<IHttpResponse<T>> => {
				const body: T = r.response
				const status: number = r.status;

				return of({body, status, error: false});
			}),
			catchError((err: AjaxError): Observable<IHttpResponse<T>> => {
				console.log(err);
				return of({error: true, errorMsg: err.message, status: err.status});
			})
		);
	}

	static post_r = <T> (url: string, data: any, headers?: RequestHeaders): Observable<IHttpResponse<T>> => {
		const defaultHeaders: RequestHeaders = HttpClient.getHeaders();
		const _headers = {...defaultHeaders, ...headers};

		return ajax.post(url, data, _headers).pipe(
			switchMap((r: AjaxResponse): Observable<IHttpResponse<T>> => {
				const body: T = r.response
				const status: number = r.status;

				return of({body, status, error: false});
			}),
			catchError((err: AjaxError): Observable<IHttpResponse<T>> => {
				console.log(err);
				return of({error: true, errorMsg: err.message, status: err.status});
			})
		);
	}

	static put_r = <T> (url: string, data: any, headers?: RequestHeaders): Observable<IHttpResponse<T>> => {
		const defaultHeaders: RequestHeaders = HttpClient.getHeaders();
		const _headers = {...defaultHeaders, ...headers};

		return ajax.put(url, data, _headers).pipe(
			switchMap((r: AjaxResponse): Observable<IHttpResponse<T>> => {
				const body: T = r.response
				const status: number = r.status;

				return of({body, status, error: false});
			}),
			catchError((err: AjaxError): Observable<IHttpResponse<T>> => {
				console.log(err);
				return of({error: true, errorMsg: err.message, status: err.status});
			})
		);
	}

	static delete_r = <T> (url: string, headers?: RequestHeaders): Observable<IHttpResponse<T>> => {
		const defaultHeaders: RequestHeaders = HttpClient.getHeaders();
		const _headers = {...defaultHeaders, ...headers};

		return ajax.delete(url, _headers).pipe(
			switchMap((r: AjaxResponse) => {
				const body: T = r.response
				const status: number = r.status;
				return of({body, status, error: false});
			}),
			catchError((err: AjaxError): Observable<IHttpResponse<T>> => {
				console.log(err);
				return of({error: true, errorMsg: err.message, status: err.status});
			})
		);
	}
}

export default HttpClient;
