import React from "react";
import "./App.css";
import "rsuite/dist/styles/rsuite-default.css";
import Router from "./Components/router/router";
import routes from "./Components/router/routes";

const App = () => {
	return <Router routes={routes}/>;
};

export default App;
