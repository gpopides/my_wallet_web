
interface UserVehicle {
	numberOfSeats: number;
	color: string;
	type: string;
}

export interface User {
	firstName: string;
	id: number;
	lastName: string;
	ssn: string;
	username: string;
	vehicle: UserVehicle
}

export interface UserResponse {
	users: Array<User>
}
